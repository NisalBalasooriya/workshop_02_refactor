import org.junit.Assert;
import org.junit.Test;


public class AardvarkTest extends Aardvark{
	  @Test
	    public void testGenerateDominoes() {
	        Aardvark aardvark = new Aardvark();
	        aardvark.generateDominoes();

	        List<Domino> dominoes = aardvark.getDominoes();
	        int count = dominoes.size();
	        Assert.assertEquals(28, count);
	    }

	    @Test
	    public void testGenerateGuesses() {
	        Aardvark aardvark = new Aardvark();
	        aardvark.generateGuesses();

	        List<Domino> guesses = aardvark.getGuesses();
	        int count = guesses.size();
	        Assert.assertEquals(28, count);
	    }

}
